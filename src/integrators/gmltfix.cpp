
/*
    pbrt source code is Copyright(c) 1998-2016
                        Matt Pharr, Greg Humphreys, and Wenzel Jakob.

    This file is part of pbrt.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are
    met:

    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.

    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
    TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
    PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

// integrators/mlt.cpp*
#include "integrators/gmltfix.h"
#include <fstream>
#include <iostream>
#include <string>
#include "camera.h"
#include "film.h"
#include "filters/box.h"
#include "integrator.h"
#include "integrators/bdpt.h"
#include "numeric"
#include "paramset.h"
#include "progressreporter.h"
#include "sampler.h"
#include "sampling.h"
#include "scene.h"
#include "stats.h"
using namespace std;
namespace pbrt {

STAT_PERCENT("Integrator/Acceptance rate", acceptedMutations, totalMutations);

// GMLTFixSampler Constants
static const int cameraStreamIndex = 0;
static const int lightStreamIndex = 1;
static const int connectionStreamIndex = 2;
static const int nSampleStreams = 3;

// GMLTFixSampler Method Definitions
Float GMLTFixIntegrator::GetTsallis(Spectrum currentL,
                                    std::vector<Spectrum> sv) {
    if (sv.size() == 0) return 0;
    if (useShannon) {
        sv.push_back(currentL);
        return GetShannon(sv);
    }
    // else if(currentL.y() > 1)
    //     return 1;
    Float q = tsallisRatio;
    const Float YWeight[3] = {0.4f, 0.3f, 0.6f};
    Float redTotal = currentL[0], greenTotal = currentL[1],
          blueTotal = currentL[2], I_SUM = currentL.y();
    for (int i = 0; i < sv.size(); i++) {
        redTotal += sv[i][0];
        greenTotal += sv[i][1];
        blueTotal += sv[i][2];
        I_SUM += sv[i].y();
        // if(sv[i].y() > 1e-6)
        // {
        //     printf("%.10f \n", sv[i].y());
        // }
        // std::cout<<sv[i].y()<<"---sv[i].y()---"<<std::endl;
        // fv[i] = YWeight[0] * sv[i][0] + YWeight[1] * sv[i][1] + YWeight[2] *
        // sv[i][2];
    }
    Float Hmax = (1 / 1 - q) * (pow(sv.size() + 1, 1 - q) - 1);
    Float PR = pow(abs(redTotal) <= 1e-6 ? (1.0 / (sv.size() + 1))
                                         : (currentL[0] / redTotal),
                   q),
          PG = pow(abs(greenTotal) <= 1e-6 ? (1.0 / (sv.size() + 1))
                                           : (currentL[1] / greenTotal),
                   q),
          PB = pow(abs(blueTotal) <= 1e-6 ? (1.0 / (sv.size() + 1))
                                          : (currentL[2] / blueTotal),
                   q);
    for (int i = 0; i < sv.size(); i++) {
        PR += pow(abs(redTotal) <= 1e-6 ? (1.0 / (sv.size() + 1))
                                        : (sv[i][0] / redTotal),
                  q);
        PG += pow(abs(greenTotal) <= 1e-6 ? (1.0 / (sv.size() + 1))
                                          : (sv[i][1] / greenTotal),
                  q);
        PB += pow(abs(blueTotal) <= 1e-6 ? (1.0 / (sv.size() + 1))
                                         : (sv[i][2] / blueTotal),
                  q);
    }

    Float QR = (PR - 1) / (pow(sv.size() + 1, 1 - q) - 1);
    Float QG = (PG - 1) / (pow(sv.size() + 1, 1 - q) - 1);
    Float QB = (PB - 1) / (pow(sv.size() + 1, 1 - q) - 1);
    Float Q =
        ((YWeight[0] * QR) + (YWeight[1] * QG) + (YWeight[2] * QB)) / 1.3f;
    Float I_AVG = I_SUM / (sv.size() + 1);
    Float b = 1 / (pow(Q, 1.0 / 3.0) + 1) * currentL.y();
    return b;
}

Float GMLTFixIntegrator::GetShannon(std::vector<Spectrum> sv) {
    std::vector<Float> temp(256, 0);
    for (int i = 0; i < sv.size(); i++) {
        int j = (int)((sv[i][0] * 299 + sv[i][1] * 587 + sv[i][2] * 114) * 255 /
                      1000);
        if (j > 255) j = 255;
        temp[j] += 1;
    }
    for (int i = 0; i < 256; i++) {
        temp[i] = temp[i] / sv.size();
    }
    Float result = 0.0f;
    for (int i = 0; i < 256; i++) {
        if (temp[i] > 0.0f) {
            result -= (temp[i] * (log(temp[i]) / log(2.0)));
        }
    }
    Float temp2 = 1.0 / sv.size();
    Float maxShannon2 = -(temp2 * (log(temp2) / log(2.0)) * sv.size());
    return result / maxShannon2;
}

Spectrum GMLTFixSampler3(float abs) {
    // printf("abs --> %.2f \n", abs);
    if (abs > 0.5) {
        abs = 0.5;
    }
    int RgbTemp = (abs * 3990) + 164;
    int a = RgbTemp / 360;
    int b = RgbTemp / 60 / 36;
    int c = RgbTemp / 60 % 6;

    float p = 0;
    float q = 1 - b;

    float RTemp = 0;
    float GTemp = 0;
    float BTemp = 0;

    switch (a) {
    case 0:
        RTemp = 0;
        GTemp = 1;
        BTemp = 1;
        break;
    case 1:
        RTemp = 0;
        GTemp = 1;
        BTemp = 1;
        break;
    case 2:
        RTemp = 0;
        GTemp = 1;
        BTemp = q / 2;
        break;
    case 3:
        RTemp = 0;
        GTemp = q / 2;
        BTemp = 1;
        break;
    case 4:
        RTemp = 1;
        GTemp = 0;
        BTemp = q;
        break;
    case 5:
        RTemp = 1;
        GTemp = 1;
        BTemp = 0;
        break;
    }
    Float rgb[3] = {RTemp, GTemp, BTemp};
    return RGBSpectrum::FromRGB(rgb);
}

void GMLTFixIntegrator::Render(const Scene &scene) {
    std::unique_ptr<Distribution1D> lightDistr =
        ComputeLightPowerDistribution(scene);
    int width = (int)camera->film->GetSampleBounds().pMax.x,
        height = (int)camera->film->GetSampleBounds().pMax.y;
    pixcelSamplers = vector<int>(width * height, 0);
    // Compute a reverse mapping from light pointers to offsets into the
    // scene lights vector (and, equivalently, offsets into
    // lightDistr). Added after book text was finalized; this is critical
    // to reasonable performance with 100s+ of light sources.
    std::unordered_map<const Light *, size_t> lightToIndex;
    for (size_t i = 0; i < scene.lights.size(); ++i)
        lightToIndex[scene.lights[i].get()] = i;
    Film &film = *camera->film;
    // Generate bootstrap samples and compute normalization constant $b$
    int nBootstrapSamples = nBootstrap * (maxDepth + 1);
    std::vector<Float> bootstrapWeights(nBootstrapSamples, 0), Var;
    std::vector<Float> bootstrapTsallis(nBootstrapSamples, 0);
    std::vector<Float> bootstrapContribution(nBootstrapSamples, 0);
    std::vector<Spectrum> filmTsallis(width * height);
    std::vector<std::vector<Spectrum>> sv(nBootstrapSamples,
                                          std::vector<Spectrum>());
    vector<shared_ptr<GMLTSampler>> samplerVV(nBootstrapSamples);
    Float t_SUM = 0;
    int validNum = 0;
    float SumV = 0.f;
    int _index = 0;
    if (scene.lights.size() > 0) {
        std::vector<MemoryArena> bootstrapThreadArenas(MaxThreadIndex());
        ProgressReporter progress1(width * height / 256,
                                   "first bootstrap paths");
        ParallelFor(
            [&](int i) {
                // Generate _i_th bootstrap sample
                GMLTSampler sampler(mutationsPerPixel, i, sigma,
                                    largeStepProbability, nSampleStreams);
                MemoryArena &arena = bootstrapThreadArenas[ThreadIndex];
                Point2f pRaster(i / height, i % height);
                sampler.SetJitter(true);
                Spectrum currentL =
                    L(scene, arena, lightDistr, lightToIndex, sampler,
                      rand() % (maxDepth - 1) + 1, &pRaster);
                sampler.SetJitter(false);
                if ((i + 1) % 256 == 0) progress1.Update();
                if (currentL.y() > 0) {
                    filmTsallis[i] = currentL / currentL.y();
                    camera->film->AddSplat(pRaster, currentL / currentL.y());
                } else
                    filmTsallis[i] = Spectrum(0.0f);
            },
            width * height);
        progress1.Done();
    }
    if (scene.lights.size() > 0) {
        ProgressReporter progress(nBootstrap / 256,
                                  "Generating bootstrap paths");
        std::vector<MemoryArena> bootstrapThreadArenas(MaxThreadIndex());
        int chunkSize = Clamp(nBootstrap / 128, 1, 8192);
        ParallelFor(
            [&](int i) {
                // Generate _i_th bootstrap sample
                MemoryArena &arena = bootstrapThreadArenas[ThreadIndex];
                for (int depth = 0; depth <= maxDepth; ++depth) {
                    int rngIndex = i * (maxDepth + 1) + depth;
                    shared_ptr<GMLTSampler> sampler(
                        new GMLTSampler(mutationsPerPixel, rngIndex, sigma,
                                        largeStepProbability, nSampleStreams));
                    Point2f pRaster;
                    Spectrum Light = L(scene, arena, lightDistr, lightToIndex,
                                       *sampler, depth, &pRaster);
                    Float tsallis = 0;
                    if (!InsideExclusive((Point2i)pRaster,
                                         camera->film->croppedPixelBounds) ||
                        !Light.y() > 0) {
                        tsallis = 0;
                    } else {
                        for (int j = 0; j < pixelSamplerNum + 1; j++) {
                            for (int k = 0; k < pixelSamplerNum + 1; k++) {
                                Point2f nRaster;
                                Point2f offset;
                                offset =
                                    Point2f(j - 0.5f - pixelSamplerNum / 2,
                                            k - 0.5f - pixelSamplerNum / 2);
                                nRaster = pRaster + offset;
                                int x = (int)nRaster.x, y = (int)nRaster.y;
                                if (!InsideExclusive(
                                        (Point2i)nRaster,
                                        camera->film->croppedPixelBounds)) {
                                } else {
                                    sv[rngIndex].push_back(
                                        filmTsallis[x * height + y]);
                                }
                            }
                        }
                        tsallis = GetTsallis(Light, sv[rngIndex]);
                    }
                    if (tsallis > 0) {
                        t_SUM += tsallis;
                        validNum++;
                    }
                    bootstrapContribution[rngIndex] = Light.y() * tsallis;
                    bootstrapTsallis[rngIndex] = tsallis;
                    bootstrapWeights[rngIndex] = Light.y();
                    sampler->SetLightValue(Light, pRaster);
                    samplerVV[rngIndex] = sampler;
                    if (Light.y() > 0) {
                        film.AddSplat(pRaster, Light / Light.y());
                    }
                    arena.Reset();
                }
                if ((i + 1) % 256 == 0) progress.Update();
            },
            nBootstrap, chunkSize);
        progress.Done();
    }
    Distribution1D bootstrap(&bootstrapWeights[0], nBootstrapSamples);
    Distribution1D tsallisbootstrap(&bootstrapTsallis[0], nBootstrapSamples);
    Distribution1D wightsbootstrap(&bootstrapContribution[0],
                                   nBootstrapSamples);
    Float b = bootstrap.funcInt * (maxDepth + 1);
    Float _b = wightsbootstrap.funcInt;
    Float H_Avg = GetShannon(filmTsallis) * _b;
    for (uint32_t i = 0; i < nBootstrap; ++i) {
        Float Va =
            (_b - bootstrapContribution[i]) * (_b - bootstrapContribution[i]);
        SumV += Va;
        Var.push_back(Va);
    }
    double sum = accumulate(std::begin(bootstrapWeights),
                            std::end(bootstrapWeights), 0.0);
    double mean = sum / bootstrapWeights.size();  //均值

    double accum = 0.0;
    std::for_each(std::begin(bootstrapWeights), std::end(bootstrapWeights),
                  [&](const double d) { accum += (d - mean) * (d - mean); });

    double stdev = sqrt(accum / (bootstrapWeights.size() - 1));  //方差
    // Run _nChains_ Markov chains in parallel

    int64_t nTotalMutations =
        (int64_t)mutationsPerPixel * (int64_t)film.GetSampleBounds().Area();
    const int progressFrequency = 32768;
    int threadMutations = nTotalMutations / MaxThreadIndex();
    int chainsTotal = std::sqrt(threadMutations * 0.4);
    int64_t nChainMutations = threadMutations / chainsTotal;
    RNG rng(chainsTotal);
    if (scene.lights.size() > 0) {
        const int progressFrequency = 32768;
        ProgressReporter progress(chainsTotal * MaxThreadIndex(), "Rendering");
        ParallelFor(
            [&](int index) {
                vector<vector<shared_ptr<GMLTSampler>>> samplerV(chainsTotal + 1);
                RNG rng(index);
                int finalIndex = 0;
                float zCurrent;
                int clearIndex = 0;
                int salt = 20;
                if (finalIndex == 0) {
                    float sumW = 0;
                    float sumZ = 0;
                    // Run the Markov chain for _nChainMutations_ steps
                    vector<float> weightV;
                    vector<float> vV;
                    MemoryArena arena;
                    vector<shared_ptr<GMLTSampler>> samplers;
                    for (int64_t j = 0; j < nChainMutations; j++) {
                        float randomNum = rng.UniformFloat();
                        int bootstrapIndex = wightsbootstrap.SampleDiscrete(
                            randomNum, nullptr, nullptr, false);
                        shared_ptr<GMLTSampler> sampler(
                            new GMLTSampler(*samplerVV[bootstrapIndex]));
                        samplers.push_back(sampler);
                        sampler->StartIteration();
                        // chainsSampler.push_back(sampler);
                        int depth = sampler->rngSequenceIndex % (maxDepth + 1);
                        Point2f pProposed;
                        Spectrum LCurrent;
                        Point2f pCurrent;
                        Spectrum LProposed =
                            L(scene, arena, lightDistr, lightToIndex, *sampler,
                              depth, &pProposed);
                        ++totalMutations;
                        LCurrent = sampler->GetLightValue().L;
                        pCurrent = sampler->GetLightValue().p;
                        sampler->LightBackup();
                        sampler->SetLightValue(LProposed, pProposed);
                        sumW += LProposed.y();
                        Float x = LProposed.y();
                        // Compute acceptance probability for proposed sample
                        float weight = 0;
                        float v = 1 / (stdev * sqrt(2 * 3.14159f)) *
                                  pow(2.71828f, -(pow(LProposed.y() - mean, 2) /
                                                  (2 * stdev * stdev)));
                        // Splat both current and proposed samples to _film_
                        weight = v;
                        weightV.push_back(weight);
                        vV.push_back(v < 0.000000001f ? 0.000000001f : v);
                        sumZ += weight;
                        if (InsideExclusive((Point2i)pProposed,
                                            film.croppedPixelBounds)) {
                            pixcelSamplers[(int)pProposed.x * height +
                                           (int)pProposed.y]++;
                        }
                        Float accept =
                            std::min((Float)1, LProposed.y() / LCurrent.y());
                        if (accept > 0) {
                            film.AddSplat(pProposed,
                                          LProposed * accept / LProposed.y());
                            sampler->Accept();
                        } else {
                            sampler->Reject();
                            sampler->LargeReject();
                        }
                        if (rng.UniformFloat() < accept) sampler->accept = true;
                        film.AddSplat(pCurrent,
                                      LCurrent * (1 - accept) / LCurrent.y());
                        // Accept or reject the proposal
                    }
                    float zInv = 1 / sumZ;
                    float sumZest = 0;
                    for (int i = 0; i < weightV.size(); i++) {
                        float zest = zInv * weightV[i] / vV[i];
                        sumZest += zest;
                    }
                    zCurrent = sumZest / nChainMutations;
                    vector<float>().swap(weightV);
                    vector<float>().swap(vV);
                    finalIndex = 0;
                    samplerV[finalIndex] = samplers;
                }

                for (int64_t i = 1; i < chainsTotal; i++) {
                    float sumW = 0;
                    float sumZ = 0;
                    // Run the Markov chain for _nChainMutations_ steps
                    vector<float> weightV;
                    vector<float> vV;
                    MemoryArena arena;
                    vector<shared_ptr<GMLTSampler>> samplers;
                    for (int64_t j = 0; j < nChainMutations; j++) {
                        shared_ptr<GMLTSampler> sampler(
                            new GMLTSampler(*samplerV[finalIndex][j]));
                        samplers.push_back(sampler);
                        sampler->StartIteration();
                        int depth = sampler->rngSequenceIndex % (maxDepth + 1);
                        Float tCurrent =
                            GetTsallis(sampler->GetLightValue().L, sv[sampler->rngSequenceIndex]);
                        sampler->SetLargeStepProbability(
                            tCurrent - useShannon ? b : b);
                        Point2f pProposed;
                        Spectrum LCurrent;
                        Point2f pCurrent;
                        Spectrum LProposed =
                            L(scene, arena, lightDistr, lightToIndex, *sampler,
                              depth, &pProposed);
                        ++totalMutations;
                        LCurrent = sampler->GetLightValue().L;
                        pCurrent = sampler->GetLightValue().p;
                        sampler->LightBackup();
                        sampler->SetLightValue(LProposed, pProposed);
                        sumW += LProposed.y();
                        Float x = LProposed.y();
                        // Compute acceptance probability for proposed sample
                        float weight = 0;
                        float v = 1 / (stdev * sqrt(2 * 3.14159f)) *
                                  pow(2.71828f, -(pow(LProposed.y() - mean, 2) /
                                                  (2 * stdev * stdev)));
                        // Splat both current and proposed samples to _film_
                        weight = pow(2.71828f, -(pow((x * x - (2 * mean)), 2) /
                                                 (2 * mean * mean)));
                        // cout<<v<<"<--v       "<<weight<<"<--weight"<<endl;
                        weightV.push_back(weight);
                        vV.push_back(v < 0.000000001f ? 0.000000001f : v);
                        sumZ += weight;
                        if (InsideExclusive((Point2i)pProposed,
                                            film.croppedPixelBounds)) {
                            pixcelSamplers[(int)pProposed.x * height +
                                           (int)pProposed.y]++;
                        }
                        Float accept =
                            std::min((Float)1, LProposed.y() / LCurrent.y());
                        if (accept > 0) {
                            film.AddSplat(pProposed,
                                          LProposed * accept / LProposed.y());
                        }
                        if (rng.UniformFloat() < accept) sampler->accept = true;
                        film.AddSplat(pCurrent,
                                      LCurrent * (1 - accept) / LCurrent.y());
                        // Accept or reject the proposal
                        _index++;
                        if (_index % (width * height) == 0 &&
                            PbrtOptions.sequence) {
                            camera->film->WriteCurrentImage(
                                b / (_index / (width * height)),
                                std::to_string(_index / (width * height)));
                        }
                    }
                    float zInv = 1 / sumZ;
                    float sumZest = 0;
                    for (int i = 0; i < weightV.size(); i++) {
                        float zest = zInv * weightV[i] / vV[i];
                        sumZest += zest;
                    }
                    float z = sumZest / nChainMutations;
                    float zAccept = std::min(1.0f, z / zCurrent);
                    if (rng.UniformFloat() < zAccept) {
                        for (int k = 0; k < nChainMutations; k++) {
                            shared_ptr<GMLTSampler> samplerTemp = samplers[k];
                            if (samplerTemp->accept) {
                                ++acceptedMutations;
                                samplerTemp->Accept();
                            } else {
                                samplerTemp->Reject();
                                samplerTemp->LargeReject();
                            }
                        }
                        samplerV[i] = samplers;
                        zCurrent = z;
                        finalIndex = i;
                        if (clearIndex > salt) {
                            for (int n = i - clearIndex; n < i; n++) {
                                samplerV[n].clear();
                                samplerV[n] = vector<shared_ptr<GMLTSampler>>();
                            }
                            clearIndex = 0;
                        }
                    }
                    clearIndex++;
                    vector<float>().swap(weightV);
                    vector<float>().swap(vV);
                    progress.Update();
                }
            },
            MaxThreadIndex());
        progress.Done();
    }
    auto sampleMax =
        std::max_element(std::begin(pixcelSamplers), std::end(pixcelSamplers));
    ProgressReporter progress2(width * height / 256, "add film");
    ofstream outfile(film.filename + "_sampler.dat");
    ParallelFor(
        [&](int i) {
            // Generate _i_th bootstrap sample
            Point2f pRaster(i / height, i % height);
            camera->samplefilm->AddSplat(
                pRaster, Spectrum(pixcelSamplers[i]) / *sampleMax);
            outfile << pixcelSamplers[i] << endl;
            if ((i + 1) % 256 == 0) progress2.Update();
        },
        width * height);
    progress2.Done();
    // Store final image computed with MLT
    camera->film->WriteImage(b / mutationsPerPixel);
    camera->samplefilm->WriteImage(1);
}  // namespace pbrt

GMLTFixIntegrator *CreateGMLTFixIntegrator(const ParamSet &params,
                                           std::shared_ptr<const Camera> camera,
                                           std::shared_ptr<Sampler> sampler) {
    int maxDepth = params.FindOneInt("maxdepth", 17);
    int nBootstrap = params.FindOneInt("bootstrapsamples", 100000);
    int64_t nChains = params.FindOneInt("chains", 1000);
    int mutationsPerPixel = params.FindOneInt("mutationsperpixel", 100);
    if (PbrtOptions.spp != 0) mutationsPerPixel = PbrtOptions.spp;
    mutationsPerPixel = mutationsPerPixel * 1.2f;
    int pixelSamplerNum = params.FindOneInt("pixelSamplerNum", 8);
    bool pixelArea = params.FindOneBool("pixelArea", false);
    bool useShannon = params.FindOneBool("useShannon", true);
    bool smapleImg = params.FindOneBool("smapleImg", false);
    Float tsallisRatio = params.FindOneFloat("tsallisRatio", 2.0f);
    Float largeStepProbability =
        params.FindOneFloat("largestepprobability", 0.3f);
    Float sigma = params.FindOneFloat("sigma", .01f);
    if (PbrtOptions.quickRender) {
        mutationsPerPixel = std::max(1, mutationsPerPixel / 16);
        nBootstrap = std::max(1, nBootstrap / 16);
    }
    return new GMLTFixIntegrator(camera, maxDepth, nBootstrap, nChains,
                                 mutationsPerPixel, sigma, largeStepProbability,
                                 pixelSamplerNum, pixelArea, tsallisRatio,
                                 sampler, useShannon, smapleImg);
}

}  // namespace pbrt
