
/*
    pbrt source code is Copyright(c) 1998-2016
                        Matt Pharr, Greg Humphreys, and Wenzel Jakob.

    This file is part of pbrt.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are
    met:

    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.

    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
    TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
    PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

// integrators/mlt.cpp*
#include "integrators/gmlt.h"
#include "camera.h"
#include "film.h"
#include "filters/box.h"
#include "fstream"
#include "integrator.h"
#include "integrators/bdpt.h"
#include "numeric"
#include "paramset.h"
#include "progressreporter.h"
#include "sampler.h"
#include "sampling.h"
#include "scene.h"
#include "stats.h"
using namespace std;

namespace pbrt {

STAT_PERCENT("Integrator/Acceptance rate", acceptedMutations, totalMutations);
STAT_PERCENT("Integrator/Reject Chains rate", rejectChains, chainsTotal);
// GMLTSampler Constants
static const int cameraStreamIndex = 0;
static const int lightStreamIndex = 1;
static const int connectionStreamIndex = 2;
static const int nSampleStreams = 3;

// GMLTSampler Method Definitions
Float GMLTSampler::Get1D() {
    ProfilePhase _(Prof::GetSample);
    int index = GetNextIndex();
    EnsureReady(index);
    return X[index].value;
}

Point2f GMLTSampler::Get2D() { return {Get1D(), Get1D()}; }

std::unique_ptr<Sampler> GMLTSampler::Clone(int seed) {
    LOG(FATAL) << "GMLTSampler::Clone() is not implemented";
    return nullptr;
}
//
// void GMLTSampler::StartLargeIteration() {
//    currentIteration = 0;
//    for (int i = 0; i < X.size(); i++) {
//        X[i].LargeBackup();
//        X[i].lastModificationIteration = 0;
//        X[i].modifyBackup = 0;
//        X[i].valueBackup = 0;
//    }
//}

void GMLTSampler::StartIteration() {
    currentIteration++;
    accept = false;
    largeStep = rng.UniformFloat() < largeStepProbability;
}

void GMLTSampler::Accept() {
    if (largeStep) lastLargeStepIteration = currentIteration;
}

void GMLTSampler::EnsureReady(int index) {
    if (index >= X.size()) X.resize(index + 1);
    PrimarySample &Xi = X[index];

    // Reset $\VEC{X}_i$ if a large step took place in the meantime
    if (Xi.lastModificationIteration < lastLargeStepIteration) {
        Xi.value = rng.UniformFloat();
        Xi.lastModificationIteration = lastLargeStepIteration;
    }

    // Apply remaining sequence of mutations to _sample_
    Xi.Backup();
    if (largeStep) {
        Xi.value = rng.UniformFloat();
    } else {
        int64_t nSmall = currentIteration - Xi.lastModificationIteration;
        // Apply _nSmall_ small step mutations

        // Sample the standard normal distribution $N(0, 1)$
        Float normalSample = Sqrt2 * ErfInv(2 * rng.UniformFloat() - 1);

        // Compute the effective standard deviation and apply perturbation to
        // $\VEC{X}_i$
        Float effSigma = sigma * std::sqrt((Float)nSmall);
        Xi.value += normalSample * effSigma;
        Xi.value -= std::floor(Xi.value);
    }
    Xi.lastModificationIteration = currentIteration;
}

void GMLTSampler::Reject() {
    for (auto &Xi : X)
        if (Xi.lastModificationIteration == currentIteration) Xi.Restore();
    --currentIteration;
}

void GMLTSampler::LargeAccept() {}

//************************************
// Method:    LargeReject
// Description:采样拒绝之后回退记录的样本值
//************************************
void GMLTSampler::LargeReject() { LightRestore(); }

void GMLTSampler::StartStream(int index) {
    CHECK_LT(index, streamCount);
    streamIndex = index;
    sampleIndex = 0;
}
// MLT Method Definitions
Spectrum GMLTIntegrator::L(
    const Scene &scene, MemoryArena &arena,
    const std::unique_ptr<Distribution1D> &lightDistr,
    const std::unordered_map<const Light *, size_t> &lightToIndex,
    GMLTSampler &sampler, int depth, Point2f *pRaster) {
    sampler.StartStream(cameraStreamIndex);
    // Determine the number of available strategies and pick a specific one
    int s, t, nStrategies;
    if (depth == 0) {
        nStrategies = 1;
        s = 0;
        t = 2;
    } else {
        nStrategies = depth + 2;
        s = std::min((int)(sampler.Get1D() * nStrategies), nStrategies - 1);
        t = nStrategies - s;
    }

    // Generate a camera subpath with exactly _t_ vertices
    Vertex *cameraVertices = arena.Alloc<Vertex>(t);
    Bounds2f sampleBounds = (Bounds2f)camera->film->GetSampleBounds();
    if (!sampler.IsJitter()) {
        int height = sampleBounds.pMax.y;
        int width = sampleBounds.pMax.x;
        *pRaster = sampleBounds.Lerp(sampler.Get2D());
        int index = (int)pRaster->x * height + (int)pRaster->y;
        RNG rng(index);
        float random1 = rng.UniformFloat();
        float random2 = rng.UniformFloat();
        int left = random1 < random2 ? random1 * width * height
                                     : random2 * width * height;
        int right = random1 < random2 ? random2 * width * height
                                      : random1 * width * height;
        if (InsideExclusive((Point2i)*pRaster,
                            camera->film->croppedPixelBounds) &&
            pixcelSamplers[index] > (mutationsPerPixel * 10)) {
            auto smallest = find_if(
                std::begin(pixcelSamplers) + left,
                std::end(pixcelSamplers) + right,
                [this](float x) { return x < mutationsPerPixel * 0.1f; });
            if (smallest != pixcelSamplers.begin() + right) {
                int distance =
                    std::distance(std::begin(pixcelSamplers), smallest);
                pRaster->x = distance / height;
                pRaster->y = distance % height;
                sampler.Set2D(pRaster->x / sampleBounds.pMax.x,
                              pRaster->y / sampleBounds.pMax.y);
            }
        }
    } else
        sampler.Get2D();
    if (GenerateCameraSubpath(scene, sampler, arena, t, *camera, *pRaster,
                              cameraVertices) != t)
        return Spectrum(0.f);

    // Generate a light subpath with exactly _s_ vertices
    sampler.StartStream(lightStreamIndex);
    Vertex *lightVertices = arena.Alloc<Vertex>(s);
    if (GenerateLightSubpath(scene, sampler, arena, s, cameraVertices[0].time(),
                             *lightDistr, lightToIndex, lightVertices) != s)
        return Spectrum(0.f);

    // Execute connection strategy and return the radiance estimate
    sampler.StartStream(connectionStreamIndex);
    return ConnectBDPT(scene, lightVertices, cameraVertices, s, t, *lightDistr,
                       lightToIndex, *camera, sampler, pRaster) *
           nStrategies;
}

void GMLTIntegrator::Render(const Scene &scene) {
    std::unique_ptr<Distribution1D> lightDistr =
        ComputeLightPowerDistribution(scene);
    int width = (int)camera->film->GetSampleBounds().pMax.x,
        height = (int)camera->film->GetSampleBounds().pMax.y;
    pixcelSamplers = vector<int>(width * height, 0);
    // Compute a reverse mapping from light pointers to offsets into the
    // scene lights vector (and, equivalently, offsets into
    // lightDistr). Added after book text was finalized; this is critical
    // to reasonable performance with 100s+ of light sources.
    std::unordered_map<const Light *, size_t> lightToIndex;
    for (size_t i = 0; i < scene.lights.size(); ++i)
        lightToIndex[scene.lights[i].get()] = i;
    Film &film = *camera->film;
    // Generate bootstrap samples and compute normalization constant $b$
    int nBootstrapSamples = nBootstrap * (maxDepth + 1);
    std::vector<Float> bootstrapWeights(nBootstrapSamples, 0);
    vector<shared_ptr<GMLTSampler>> samplerVV(nBootstrapSamples);
    ofstream weightfile(film.filename + "_weight.dat");
    int _index = 0;
    if (scene.lights.size() > 0) {
        ProgressReporter progress(nBootstrap / 256,
                                  "Generating bootstrap paths");
        std::vector<MemoryArena> bootstrapThreadArenas(MaxThreadIndex());
        int chunkSize = Clamp(nBootstrap / 128, 1, 8192);
        ParallelFor(
            [&](int i) {
                // Generate _i_th bootstrap sample
                MemoryArena &arena = bootstrapThreadArenas[ThreadIndex];
                for (int depth = 0; depth <= maxDepth; ++depth) {
                    int rngIndex = i * (maxDepth + 1) + depth;
                    shared_ptr<GMLTSampler> sampler(
                        new GMLTSampler(mutationsPerPixel, rngIndex, sigma,
                                        largeStepProbability, nSampleStreams));
                    Point2f pRaster;
                    Spectrum Light = L(scene, arena, lightDistr, lightToIndex,
                                       *sampler, depth, &pRaster);
                    sampler->SetLightValue(Light, pRaster);
                    bootstrapWeights[rngIndex] = Light.y();
                    samplerVV[rngIndex] = sampler;
                    if (Light.y() > 0) {
                        film.AddSplat(pRaster, Light / Light.y());
                    }
                    arena.Reset();
                }
                if ((i + 1) % 256 == 0) progress.Update();
            },
            nBootstrap, chunkSize);
        progress.Done();
    }
    Distribution1D bootstrap(&bootstrapWeights[0], nBootstrapSamples);
    Float b = bootstrap.funcInt * (maxDepth + 1);
    double sum = accumulate(std::begin(bootstrapWeights),
                            std::end(bootstrapWeights), 0.0);
    double mean = sum / bootstrapWeights.size();  //均值

    double accum = 0.0;
    std::for_each(std::begin(bootstrapWeights), std::end(bootstrapWeights),
                  [&](const double d) { accum += (d - mean) * (d - mean); });

    double stdev = sqrt(accum / (bootstrapWeights.size() - 1));  //方差
    // Run _nChains_ Markov chains in parallel

    int64_t nTotalMutations =
        (int64_t)mutationsPerPixel * (int64_t)film.GetSampleBounds().Area();
    const int progressFrequency = 32768;
	//计算马尔科夫链数和节点数
    int threadMutations = nTotalMutations / MaxThreadIndex();
    int chainsTotal = std::sqrt(threadMutations * 0.4);
    int64_t nChainMutations = threadMutations / chainsTotal;
    RNG rng(chainsTotal);
    if (scene.lights.size() > 0) {
        const int progressFrequency = 32768;
        ProgressReporter progress(chainsTotal * MaxThreadIndex(), "Rendering");
        ParallelFor(
            [&](int index) {
                vector<vector<shared_ptr<GMLTSampler>>> samplerV(chainsTotal + 1);
                RNG rng(index);
                int finalIndex = 0;
                float zCurrent;
                int clearIndex = 0;
                int salt = 20;
				//马尔科夫链起始样本
                if (finalIndex == 0) {
                    float sumW = 0;
                    float sumZ = 0;
                    // Run the Markov chain for _nChainMutations_ steps
                    vector<float> weightV;
                    vector<float> vV;
                    MemoryArena arena;
                    vector<shared_ptr<GMLTSampler>> samplers;
                    for (int64_t j = 0; j < nChainMutations; j++) {
                        float randomNum = rng.UniformFloat();
                        int bootstrapIndex = bootstrap.SampleDiscrete(
                            randomNum, nullptr, nullptr, false);
                        shared_ptr<GMLTSampler> sampler(
                            new GMLTSampler(*samplerVV[bootstrapIndex]));
                        samplers.push_back(sampler);
                        sampler->StartIteration();
                        // chainsSampler.push_back(sampler);
                        int depth = sampler->rngSequenceIndex % (maxDepth + 1);
                        Point2f pProposed;
                        Spectrum LCurrent;
                        Point2f pCurrent;
                        Spectrum LProposed =
                            L(scene, arena, lightDistr, lightToIndex, *sampler,
                              depth, &pProposed);
                        ++totalMutations;
						//获得上次采样备份的样本值
                        LCurrent = sampler->GetLightValue().L;
                        pCurrent = sampler->GetLightValue().p;
						//备份本次采样样本值
                        sampler->LightBackup();
                        sampler->SetLightValue(LProposed, pProposed);
                        sumW += LProposed.y();
                        Float x = LProposed.y();
                        // Compute acceptance probability for proposed sample
                        float weight = 0;
						//计算采样权重
                        float v = 1 / (stdev * sqrt(2 * 3.14159f)) *
                                  pow(2.71828f, -(pow(LProposed.y() - mean, 2) /
                                                  (2 * stdev * stdev)));
                        // Splat both current and proposed samples to _film_
                        weight = v;
                        weightV.push_back(weight);
                        vV.push_back(v < 0.000000001f ? 0.000000001f : v);
                        sumZ += weight;
                        if (InsideExclusive((Point2i)pProposed,
                                            film.croppedPixelBounds)) {
                            pixcelSamplers[(int)pProposed.x * height +
                                           (int)pProposed.y]++;
                        }
                        Float accept =
                            std::min((Float)1, LProposed.y() / LCurrent.y());
                        if (accept > 0) {
                            film.AddSplat(pProposed,
                                          LProposed * accept / LProposed.y());
                            sampler->Accept();
                        } else {
                            sampler->Reject();
                            sampler->LargeReject();
                        }
                        if (rng.UniformFloat() < accept) sampler->accept = true;
                        film.AddSplat(pCurrent,
                                      LCurrent * (1 - accept) / LCurrent.y());
                        // Accept or reject the proposal
                    }
                    //计算组权重
                    float zInv = 1 / sumZ;
                    float sumZest = 0;
                    for (int i = 0; i < weightV.size(); i++) {
                        float zest = zInv * weightV[i] / vV[i];
                        sumZest += zest;
                    }
                    zCurrent = sumZest / nChainMutations;
                    vector<float>().swap(weightV);
                    vector<float>().swap(vV);
                    finalIndex = 0;
                    samplerV[finalIndex] = samplers;
                }
                //马尔科夫后续样本
                for (int64_t i = 1; i < chainsTotal; i++) {
                    float sumW = 0;
                    float sumZ = 0;
                    // Run the Markov chain for _nChainMutations_ steps
                    vector<float> weightV;
                    vector<float> vV;
                    MemoryArena arena;
                    vector<shared_ptr<GMLTSampler>> samplers;
                    for (int64_t j = 0; j < nChainMutations; j++) {
                        shared_ptr<GMLTSampler> sampler(
                            new GMLTSampler(*samplerV[finalIndex][j]));
                        samplers.push_back(sampler);
                        sampler->StartIteration();
                        int depth = sampler->rngSequenceIndex % (maxDepth + 1);
                        Point2f pProposed;
                        Spectrum LCurrent;
                        Point2f pCurrent;
                        Spectrum LProposed =
                            L(scene, arena, lightDistr, lightToIndex, *sampler,
                              depth, &pProposed);
                        ++totalMutations;
                        LCurrent = sampler->GetLightValue().L;
                        pCurrent = sampler->GetLightValue().p;
                        sampler->LightBackup();
                        sampler->SetLightValue(LProposed, pProposed);
                        sumW += LProposed.y();
                        Float x = LProposed.y();
                        // Compute acceptance probability for proposed sample
                        float weight = 0;
                        float v = 1 / (stdev * sqrt(2 * 3.14159f)) *
                                  pow(2.71828f, -(pow(LProposed.y() - mean, 2) /
                                                  (2 * stdev * stdev)));
                        // Splat both current and proposed samples to _film_
                        weight = pow(2.71828f, -(pow((x * x - (2 * mean)), 2) /
                                                 (2 * mean * mean)));
                        // cout<<v<<"<--v       "<<weight<<"<--weight"<<endl;
                        weightV.push_back(weight);
                        vV.push_back(v < 0.000000001f ? 0.000000001f : v);
                        sumZ += weight;
                        if (InsideExclusive((Point2i)pProposed,
                                            film.croppedPixelBounds)) {
                            pixcelSamplers[(int)pProposed.x * height +
                                           (int)pProposed.y]++;
                        }
                        if (mutationsPerPixel < 100)
                        weightfile << _index << "," << LProposed.y() << endl;
                        Float accept =
                            std::min((Float)1, LProposed.y() / LCurrent.y());
                        if (accept > 0) {
                            film.AddSplat(pProposed,
                                          LProposed * accept / LProposed.y());
                        }
                        if (rng.UniformFloat() < accept) sampler->accept = true;
                        film.AddSplat(pCurrent,
                                      LCurrent * (1 - accept) / LCurrent.y());
                        // Accept or reject the proposal
                        _index++;
                        if (_index % (width * height) == 0 &&
                            PbrtOptions.sequence) {
                            camera->film->WriteCurrentImage(
                                b / (_index / (width * height)),
                                std::to_string(_index / (width * height)));
                        }
                    }
                    float zInv = 1 / sumZ;
                    float sumZest = 0;
                    for (int i = 0; i < weightV.size(); i++) {
                        float zest = zInv * weightV[i] / vV[i];
                        sumZest += zest;
                    }
                    float z = sumZest / nChainMutations;
                    float zAccept = std::min(1.0f, z / zCurrent);
					//真正的接受与拒绝
                    if (rng.UniformFloat() < zAccept) {
                        for (int k = 0; k < nChainMutations; k++) {
                            shared_ptr<GMLTSampler> samplerTemp = samplers[k];
                            if (samplerTemp->accept) {
                                ++acceptedMutations;
                                samplerTemp->Accept();
                            } else {
                                samplerTemp->Reject();
                                samplerTemp->LargeReject();
                            }
                        }
                        samplerV[i] = samplers;
                        zCurrent = z;
						finalIndex = i;
						//定时清理内存空间，防止内存爆炸
                        if (clearIndex > salt) {
                            for (int n = i - clearIndex; n < i; n++) {
                                samplerV[n].clear();
                                samplerV[n] = vector<shared_ptr<GMLTSampler>>();
                            }
                            clearIndex = 0;
                        }
                    }
                    clearIndex++;
                    vector<float>().swap(weightV);
                    vector<float>().swap(vV);
                    progress.Update();
				}
            },
            MaxThreadIndex());
        progress.Done();
    }
    auto sampleMax =
        std::max_element(std::begin(pixcelSamplers), std::end(pixcelSamplers));
    ProgressReporter progress2(width * height / 256, "add film");
    ofstream outfile(film.filename + "_sampler.dat");
    ParallelFor(
        [&](int i) {
            // Generate _i_th bootstrap sample
            Point2f pRaster(i / height, i % height);
            camera->samplefilm->AddSplat(
                pRaster, Spectrum(pixcelSamplers[i]) / *sampleMax);
            outfile << pixcelSamplers[i] << endl;
            if ((i + 1) % 256 == 0) progress2.Update();
        },
        width * height);
    progress2.Done();
    // Store final image computed with MLT
    camera->film->WriteImage(b / mutationsPerPixel);
    camera->samplefilm->WriteImage(1);
}  // namespace pbrt

GMLTIntegrator *CreateGMLTIntegrator(const ParamSet &params,
                                     std::shared_ptr<const Camera> camera) {
    int maxDepth = params.FindOneInt("maxdepth", 17);
    int nBootstrap = params.FindOneInt("bootstrapsamples", 100000);
    int64_t nChains = params.FindOneInt("chains", 1000);
    int mutationsPerPixel = params.FindOneInt("mutationsperpixel", 100);
    if (PbrtOptions.spp != 0) mutationsPerPixel = PbrtOptions.spp;
    Float largeStepProbability =
        params.FindOneFloat("largestepprobability", 0.3f);
    Float sigma = params.FindOneFloat("sigma", .01f);
    if (PbrtOptions.quickRender) {
        mutationsPerPixel = std::max(1, mutationsPerPixel / 16);
        nBootstrap = std::max(1, nBootstrap / 16);
    }
    return new GMLTIntegrator(camera, maxDepth, nBootstrap, nChains,
                              mutationsPerPixel, sigma, largeStepProbability);
}

}  // namespace pbrt
