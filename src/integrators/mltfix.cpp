﻿
/*
    pbrt source code is Copyright(c) 1998-2016
                        Matt Pharr, Greg Humphreys, and Wenzel Jakob.

    This file is part of pbrt.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are
    met:

    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.

    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
    TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
    PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

// integrators/mlt.cpp*
#include "integrators/mltfix.h"
#include <fstream>
#include <iostream>
#include <string>
#include "camera.h"
#include "film.h"
#include "filters/box.h"
#include "integrator.h"
#include "integrators/bdpt.h"
#include "paramset.h"
#include "progressreporter.h"
#include "sampler.h"
#include "sampling.h"
#include "scene.h"
#include "stats.h"
using namespace std;
namespace pbrt {

STAT_PERCENT("Integrator/Acceptance rate", acceptedMutations, totalMutations);

// MLTFixSampler Constants
static const int cameraStreamIndex = 0;
static const int lightStreamIndex = 1;
static const int connectionStreamIndex = 2;
static const int nSampleStreams = 3;

// MLTFixSampler Method Definitions
Float MLTFixSampler::Get1D() {
    ProfilePhase _(Prof::GetSample);
    if (jitter) {
        return rng2.UniformFloat();
    }
    int index = GetNextIndex();
    EnsureReady(index);
    return X[index].value;
}

Point2f MLTFixSampler::Get2D() { return {Get1D(), Get1D()}; }

std::unique_ptr<Sampler> MLTFixSampler::Clone(int seed) {
    LOG(FATAL) << "MLTFixSampler::Clone() is not implemented";
    return nullptr;
}

void MLTFixSampler::SetJitter(bool isJitter) { jitter = isJitter; }

void MLTFixSampler::StartIteration() {
    currentIteration++;
    largeStep = rng.UniformFloat() < largeStepProbability;
}

void MLTFixSampler::Accept() {
    if (largeStep) lastLargeStepIteration = currentIteration;
}

void MLTFixSampler::EnsureReady(int index) {
    // Enlarge _MLTFixSampler::X_ if necessary and get current $\VEC{X}_i$
    if (index >= X.size()) X.resize(index + 1);
    PrimarySample &Xi = X[index];

    // Reset $\VEC{X}_i$ if a large step took place in the meantime
    if (Xi.lastModificationIteration < lastLargeStepIteration) {
        Xi.value = rng.UniformFloat();
        Xi.lastModificationIteration = lastLargeStepIteration;
    }

    // Apply remaining sequence of mutations to _sample_
    Xi.Backup();
    if (largeStep) {
        Xi.value = rng.UniformFloat();
    } else {
        int64_t nSmall = currentIteration - Xi.lastModificationIteration;
        // Apply _nSmall_ small step mutations

        // Sample the standard normal distribution $N(0, 1)$
        Float normalSample = Sqrt2 * ErfInv(2 * rng.UniformFloat() - 1);

        // Compute the effective standard deviation and apply perturbation to
        // $\VEC{X}_i$
        Float effSigma = sigma * std::sqrt((Float)nSmall);
        Xi.value += normalSample * effSigma;
        Xi.value -= std::floor(Xi.value);
    }
    Xi.lastModificationIteration = currentIteration;
}

void MLTFixSampler::Reject() {
    for (auto &Xi : X)
        if (Xi.lastModificationIteration == currentIteration) Xi.Restore();
    --currentIteration;
}

void MLTFixSampler::StartStream(int index) {
    CHECK_LT(index, streamCount);
    streamIndex = index;
    sampleIndex = 0;
}

//************************************
// Method:    GetTsallis
// Parameter: Spectrum currentL
// Parameter: std::vector<Spectrum> sv
// Description:输入当前采样和周围样本计算信息熵
//************************************
Float MLTFixIntegrator::GetTsallis(Spectrum currentL,
                                   std::vector<Spectrum> sv) {
    if (sv.size() == 0) return 0;
	//根据配置决定是否使用香浓熵
    if (useShannon) {
        sv.push_back(currentL);
        return GetShannon(sv);
    }
    // else if(currentL.y() > 1)
    //     return 1;
    Float q = tsallisRatio;
    const Float YWeight[3] = {0.4f, 0.3f, 0.6f};
    Float redTotal = currentL[0], greenTotal = currentL[1],
          blueTotal = currentL[2], I_SUM = currentL.y();
    for (int i = 0; i < sv.size(); i++) {
        redTotal += sv[i][0];
        greenTotal += sv[i][1];
        blueTotal += sv[i][2];
        I_SUM += sv[i].y();
        // if(sv[i].y() > 1e-6)
        // {
        //     printf("%.10f \n", sv[i].y());
        // }
        // std::cout<<sv[i].y()<<"---sv[i].y()---"<<std::endl;
        // fv[i] = YWeight[0] * sv[i][0] + YWeight[1] * sv[i][1] + YWeight[2] *
        // sv[i][2];
    }
    Float Hmax = (1 / 1 - q) * (pow(sv.size() + 1, 1 - q) - 1);
    Float PR = pow(abs(redTotal) <= 1e-6 ? (1.0 / (sv.size() + 1))
                                         : (currentL[0] / redTotal),
                   q),
          PG = pow(abs(greenTotal) <= 1e-6 ? (1.0 / (sv.size() + 1))
                                           : (currentL[1] / greenTotal),
                   q),
          PB = pow(abs(blueTotal) <= 1e-6 ? (1.0 / (sv.size() + 1))
                                          : (currentL[2] / blueTotal),
                   q);
    for (int i = 0; i < sv.size(); i++) {
        PR += pow(abs(redTotal) <= 1e-6 ? (1.0 / (sv.size() + 1))
                                        : (sv[i][0] / redTotal),
                  q);
        PG += pow(abs(greenTotal) <= 1e-6 ? (1.0 / (sv.size() + 1))
                                          : (sv[i][1] / greenTotal),
                  q);
        PB += pow(abs(blueTotal) <= 1e-6 ? (1.0 / (sv.size() + 1))
                                         : (sv[i][2] / blueTotal),
                  q);
    }

    Float QR = (PR - 1) / (pow(sv.size() + 1, 1 - q) - 1);
    Float QG = (PG - 1) / (pow(sv.size() + 1, 1 - q) - 1);
    Float QB = (PB - 1) / (pow(sv.size() + 1, 1 - q) - 1);
    Float Q =
        ((YWeight[0] * QR) + (YWeight[1] * QG) + (YWeight[2] * QB)) / 1.3f;
    Float I_AVG = I_SUM / (sv.size() + 1);
	//归一化
    Float b = 1 / (pow(Q, 1.0 / 3.0) + 1) * currentL.y();
    return b;
}

//************************************
// Method:    GetShannon
// Parameter: std::vector<Spectrum> sv
// Description:获取香浓熵
//************************************
Float MLTFixIntegrator::GetShannon(std::vector<Spectrum> sv) {
    std::vector<Float> temp(256, 0);
    for (int i = 0; i < sv.size(); i++) {
        int j = (int)((sv[i][0] * 299 + sv[i][1] * 587 + sv[i][2] * 114) * 255 /
                      1000);
        if (j > 255) j = 255;
        temp[j] += 1;
    }
    for (int i = 0; i < 256; i++) {
        temp[i] = temp[i] / sv.size();
    }
    Float result = 0.0f;
    for (int i = 0; i < 256; i++) {
        if (temp[i] > 0.0f) {
            result -= (temp[i] * (log(temp[i]) / log(2.0)));
        }
    }
    Float temp2 = 1.0 / sv.size();
    //归一化
    Float maxShannon2 = -(temp2 * (log(temp2) / log(2.0)) * sv.size());
    return result / maxShannon2;
}

// MLT Method Definitions
Spectrum MLTFixIntegrator::L(
    const Scene &scene, MemoryArena &arena,
    const std::unique_ptr<Distribution1D> &lightDistr,
    const std::unordered_map<const Light *, size_t> &lightToIndex,
    MLTFixSampler &sampler, int depth, Point2f *pRaster, Float *misWeight) {
    sampler.StartStream(cameraStreamIndex);
    // Determine the number of available strategies and pick a specific one
    int s, t, nStrategies;
    if (depth == 0) {
        nStrategies = 1;
        s = 0;
        t = 2;
    } else {
        nStrategies = depth + 2;
        s = std::min((int)(sampler.Get1D() * nStrategies), nStrategies - 1);
        t = nStrategies - s;
    }
    // Generate a camera subpath with exactly _t_ vertices
    Vertex *cameraVertices = arena.Alloc<Vertex>(t);
    if (!sampler.IsJitter()) {
        Bounds2f sampleBounds = (Bounds2f)camera->film->GetSampleBounds();
        *pRaster = sampleBounds.Lerp(sampler.Get2D());
    } else {
        sampler.Get2D();
    }
    if (GenerateCameraSubpath(scene, sampler, arena, t, *camera, *pRaster,
                              cameraVertices) != t)
        return Spectrum(0.f);

    // Generate a light subpath with exactly _s_ vertices
    sampler.StartStream(lightStreamIndex);
    Vertex *lightVertices = arena.Alloc<Vertex>(s);
    if (GenerateLightSubpath(scene, sampler, arena, s, cameraVertices[0].time(),
                             *lightDistr, lightToIndex, lightVertices) != s)
        return Spectrum(0.f);

    // Execute connection strategy and return the radiance estimate
    sampler.StartStream(connectionStreamIndex);
    return ConnectBDPT(scene, lightVertices, cameraVertices, s, t, *lightDistr,
                       lightToIndex, *camera, sampler, pRaster, misWeight) *
           nStrategies;
}


//************************************
// Method:    GetSamplePixcel
// Parameter: float abs 像素值
// Description:获取采样分布图改像素的颜色
//************************************
Spectrum GetSamplePixcel(float abs) {
    // printf("abs --> %.2f \n", abs);
    if (abs > 0.5) {
        abs = 0.5;
    }
    int RgbTemp = (abs * 3990) + 164;
    int a = RgbTemp / 360;
    int b = RgbTemp / 60 / 36;
    int c = RgbTemp / 60 % 6;

    float p = 0;
    float q = 1 - b;

    float RTemp = 0;
    float GTemp = 0;
    float BTemp = 0;

    switch (a) {
    case 0:
        RTemp = 0;
        GTemp = 1;
        BTemp = 1;
        break;
    case 1:
        RTemp = 0;
        GTemp = 1;
        BTemp = 1;
        break;
    case 2:
        RTemp = 0;
        GTemp = 1;
        BTemp = q / 2;
        break;
    case 3:
        RTemp = 0;
        GTemp = q / 2;
        BTemp = 1;
        break;
    case 4:
        RTemp = 1;
        GTemp = 0;
        BTemp = q;
        break;
    case 5:
        RTemp = 1;
        GTemp = 1;
        BTemp = 0;
        break;
    }
    Float rgb[3] = {RTemp, GTemp, BTemp};
    return RGBSpectrum::FromRGB(rgb);
}

void MLTFixIntegrator::Render(const Scene &scene) {
    std::unique_ptr<Distribution1D> lightDistr =
        ComputeLightPowerDistribution(scene);

    // Compute a reverse mapping from light pointers to offsets into the
    // scene lights vector (and, equivalently, offsets into
    // lightDistr). Added after book text was finalized; this is critical
    // to reasonable performance with 100s+ of light sources.
    std::unordered_map<const Light *, size_t> lightToIndex;
    for (size_t i = 0; i < scene.lights.size(); ++i)
        lightToIndex[scene.lights[i].get()] = i;

    // Generate bootstrap samples and compute normalization constant $b$
    int nBootstrapSamples = nBootstrap * (maxDepth + 1);
    std::vector<Float> bootstrapWeights(nBootstrapSamples, 0), Var;//预采样权重
    std::vector<Float> bootstrapTsallis(nBootstrapSamples, 0);//预采样信息熵
    std::vector<Float> bootstrapContribution(nBootstrapSamples, 0);//预采样亮度
    int width = (int)camera->film->GetSampleBounds().pMax.x,
        height = (int)camera->film->GetSampleBounds().pMax.y;
    std::vector<Spectrum> filmTsallis(width * height);
    std::vector<Float> pixcelSamplers(width * height, 1);//像素点采样数量
    std::vector<Float> _pixcelSamplers(width * height, 1);//像素点采样数量
    std::vector<std::vector<Spectrum>> sv(nBootstrapSamples,
                                          std::vector<Spectrum>());
    Float t_SUM = 0;
    int validNum = 0;
    float SumV = 0.f;
    int index = 0;
	//预采样
    if (scene.lights.size() > 0) {
        std::vector<MemoryArena> bootstrapThreadArenas(MaxThreadIndex());
        ProgressReporter progress1(width * height / 256,
                                   "first bootstrap paths");
        ParallelFor(
            [&](int i) {
                // Generate _i_th bootstrap sample
                MLTFixSampler sampler(mutationsPerPixel, i, sigma,
                                      largeStepProbability, nSampleStreams);
                MemoryArena &arena = bootstrapThreadArenas[ThreadIndex];
                Point2f pRaster(i / height, i % height);
                Float misWeight;
                sampler.SetJitter(true);
                Spectrum currentL =
                    L(scene, arena, lightDistr, lightToIndex, sampler,
                      rand() % (maxDepth - 1) + 1, &pRaster, &misWeight);
                sampler.SetJitter(false);
                if ((i + 1) % 256 == 0) progress1.Update();
                if (currentL.y() > 0) {
                    filmTsallis[i] = currentL / currentL.y();
                    camera->film->AddSplat(pRaster, currentL / currentL.y());
                } else
                    filmTsallis[i] = Spectrum(0.0f);
            },
            width * height);
        progress1.Done();
    }
    if (scene.lights.size() > 0) {
        std::vector<MemoryArena> bootstrapThreadArenas(MaxThreadIndex());
        int chunkSize = Clamp(nBootstrap / 128, 1, 8192);
        ProgressReporter progress(nBootstrap / 256,
                                  "Generating bootstrap paths");
        ParallelFor(
            [&](int i) {
                // Generate _i_th bootstrap sample
                MemoryArena arena;
                for (int depth = 0; depth <= maxDepth; ++depth) {
                    int rngIndex = i * (maxDepth + 1) + depth;
                    MLTFixSampler sampler(mutationsPerPixel, rngIndex, sigma,
                                          largeStepProbability, nSampleStreams);
                    Point2f pRaster;
                    Float misWeight;

                    clock_t start, end;
                    start = clock();
                    Spectrum currentL =
                        L(scene, arena, lightDistr, lightToIndex, sampler,
                          depth, &pRaster, &misWeight);
                    end = clock();
                    double endtime = (double)(end - start) / CLOCKS_PER_SEC;
                    // std::cout<<"Total time:"<<endtime*1000<<"ms"<<std::endl;
                    sampler.SetJitter(true);
                    start = clock();
                    sampler.SetJitter(false);
                    Float tsallis = 0;
					//计算信息熵，因为边缘部分可能溢出所以有边界判断
					//todo:补充padding使边缘部分也可以得到预设个数像素来计算信息熵
                    if (!InsideExclusive((Point2i)pRaster,
                                         camera->film->croppedPixelBounds) ||
                        !currentL.y() > 0) {
                        tsallis = 0;
                    } else {
                        for (int j = 0; j < pixelSamplerNum + 1; j++) {
                            for (int k = 0; k < pixelSamplerNum + 1; k++) {
                                Point2f nRaster;
                                Point2f offset;
                                offset =
                                    sampler.Get2D() +
                                    Point2f(j - 0.5f - pixelSamplerNum / 2,
                                            k - 0.5f - pixelSamplerNum / 2);
                                nRaster = pRaster + offset;
                                int x = (int)nRaster.x, y = (int)nRaster.y;
                                if (!InsideExclusive(
                                        (Point2i)nRaster,
                                        camera->film->croppedPixelBounds)) {
                                } else {
                                    sv[rngIndex].push_back(
                                        filmTsallis[x * height + y]);
                                }
                            }
                        }
                        tsallis = GetTsallis(currentL, sv[rngIndex]);
                    }

                    end = clock();
                    endtime = (double)(end - start) / CLOCKS_PER_SEC;
                    // std::cout<<"Total time:"<<endtime*1000<<"ms"<<std::endl;
                    if (tsallis > 0) {
                        t_SUM += tsallis;
                        validNum++;
                    }
                    bootstrapContribution[rngIndex] = currentL.y();
                    bootstrapTsallis[rngIndex] = tsallis;
                    bootstrapWeights[rngIndex] = currentL.y() * tsallis;
                }
                if ((i + 1) % 256 == 0) progress.Update();
            },
            nBootstrap, chunkSize);
        progress.Done();
    }
    Float t_AVG = t_SUM / validNum;
    auto maxest = std::max_element(std::begin(bootstrapTsallis),
                                   std::end(bootstrapTsallis));
    Float sigama = (*maxest - t_AVG) / 4;
    for (int i = 0; i < nBootstrapSamples; i++) {
        if (bootstrapTsallis[i] > t_AVG + 2 * sigama) {
            // bootstrapWeights[i] = 0;
        }
    }
    Distribution1D bootstrap(&bootstrapWeights[0], nBootstrapSamples);
    Distribution1D tsallisbootstrap(&bootstrapTsallis[0], nBootstrapSamples);
    Distribution1D wightsbootstrap(&bootstrapContribution[0],
                                   nBootstrapSamples);
    Float b = tsallisbootstrap.funcInt;
    Float _b = wightsbootstrap.funcInt * (maxDepth + 1);
    Float H_Avg = GetShannon(filmTsallis) * _b;
    for (uint32_t i = 0; i < nBootstrap; ++i) {
        Float Va =
            (_b - bootstrapContribution[i]) * (_b - bootstrapContribution[i]);
        SumV += Va;
        Var.push_back(Va);
    }
    printf("%.2f \n", _b);
    printf("%.2f \n", b);
    // Run _nChains_ Markov chains in parallel
    Film &film = *camera->film;
    Float c_sum = 0.0f;
    int64_t nTotalMutations =
        (int64_t)mutationsPerPixel * (int64_t)film.GetSampleBounds().Area();
    if (scene.lights.size() > 0) {
        const int progressFrequency = 32768;
        ProgressReporter progress(nTotalMutations / progressFrequency,
                                  "Rendering");
        ParallelFor(
            [&](int i) {
                int64_t nChainMutations =
                    std::min((i + 1) * nTotalMutations / nChains,
                             nTotalMutations) -
                    i * nTotalMutations / nChains;
                // Follow {i}th Markov chain for _nChainMutations_
                MemoryArena arena;

                // Select initial state from the set of bootstrap samples
                RNG rng(i);
                float varianoffset = rng.UniformFloat() * SumV;
                if (Var[i] < varianoffset) {
                    int bootstrapIndex =
                        bootstrap.SampleDiscrete(rng.UniformFloat());
                    int depth = bootstrapIndex % (maxDepth + 1);
                    // Initialize local variables for selected state
                    MLTFixSampler sampler(mutationsPerPixel, bootstrapIndex,
                                          sigma, largeStepProbability,
                                          nSampleStreams);
                    Point2f pCurrent;
                    Float misWeightCurrent;
                    Spectrum LCurrent =
                        L(scene, arena, lightDistr, lightToIndex, sampler,
                          depth, &pCurrent, &misWeightCurrent);
                    Float tCurrent = GetTsallis(LCurrent, sv[bootstrapIndex]);
                    // Run the Markov chain for _nChainMutations_ steps
                    for (int64_t j = 0; j < nChainMutations; ++j) {
                        sampler.StartIteration();
                        // sampler.SetLargeStepProbability(std::min(0.0f,
                        // tCurrent - t_AVG));
                        sampler.SetLargeStepProbability(
                            tCurrent - useShannon ? b : b);
                        // printf("%.2f \n",tCurrent - t_AVG);
                        Point2f pProposed;
                        Float misWeightProposed;
                        std::vector<Spectrum> svProposed;
                        Float tProposed = 0;
                        Spectrum LProposed =
                            L(scene, arena, lightDistr, lightToIndex, sampler,
                              depth, &pProposed, &misWeightProposed);
                        if (!InsideExclusive((Point2i)pProposed,
                                             film.croppedPixelBounds)) {
                            tProposed = 0;
                        } else {
                            for (int j = 0; j < pixelSamplerNum + 1; j++) {
                                for (int k = 0; k < pixelSamplerNum + 1; k++) {
                                    Point2f nRaster;
                                    Point2f offset;
                                    offset =
                                        Point2f(j - 0.5f - pixelSamplerNum / 2,
                                                k - 0.5f - pixelSamplerNum / 2);
                                    nRaster = pProposed + offset;
                                    int x = (int)nRaster.x, y = (int)nRaster.y;
                                    if (!InsideExclusive(
                                            (Point2i)nRaster,
                                            film.croppedPixelBounds)) {
                                    } else {
                                        svProposed.push_back(
                                            filmTsallis[x * height + y] /
                                            (1 + (totalMutations /
                                                  (height * width))) *
                                            _b);
                                    }
                                }
                            }
                            tProposed = GetTsallis(LProposed, svProposed);
                        }
                        Float accept =
                            std::min((Float)1, LProposed.y() / LCurrent.y());
						//相比MLT在更新画布的同时需要更新像素点采样数还有当前点的权重。
                        if (accept > 0) {
                            film.AddSplat(pProposed,
                                          LProposed * accept / LProposed.y());
                            if (InsideExclusive((Point2i)pProposed,
                                                film.croppedPixelBounds)) {
                                filmTsallis[(int)pProposed.x * height +
                                            (int)pProposed.y] +=
                                    LProposed * accept / LProposed.y();
                                pixcelSamplers[(int)pProposed.x * height +
                                               (int)pProposed.y] += accept;
                                _pixcelSamplers[(int)pProposed.x * height +
                                                (int)pProposed.y] +=
                                    LProposed.y() * accept / _b;
                            }
                        }
                        film.AddSplat(pCurrent,
                                      LCurrent * (1 - accept) / LCurrent.y());
                        if (InsideExclusive((Point2i)pCurrent,
                                            film.croppedPixelBounds)) {
                            filmTsallis[(int)pCurrent.x * height +
                                        (int)pCurrent.y] +=
                                LCurrent * (1 - accept) / LCurrent.y();
                            pixcelSamplers[(int)pCurrent.x * height +
                                           (int)pCurrent.y] += (1 - accept);
                            _pixcelSamplers[(int)pCurrent.x * height +
                                            (int)pCurrent.y] +=
                                LCurrent.y() * (1 - accept) / _b;
                        }
                        if (rng.UniformFloat() < accept) {
                            pCurrent = pProposed;
                            LCurrent = LProposed;
                            misWeightCurrent = misWeightProposed;
                            tCurrent = tProposed;
                            sampler.Accept();
                            // std::cout<<bootstrapWeights[bootstrapIndex]<<"---bootstrapWeights---"<<LCurrent.y()<<"---LCurrent---"<<GetShannon(LProposed,
                            // sv[bootstrapIndex])<<"GetShannon(LProposed,
                            // sv[bootstrapIndex])"<<std::endl;
                            ++acceptedMutations;
                        } else
                            sampler.Reject();
                        ++totalMutations;
                        if ((i * nTotalMutations / nChains + j) %
                                progressFrequency ==
                            0)
                            progress.Update();
                        index++;
                        if (index % (width * height) == 0 &&
                            PbrtOptions.sequence) {
                            camera->film->WriteCurrentImage(
                                _b / (index / (width * height)),
                                std::to_string(index / (width * height)));
                        }
                        arena.Reset();
                    }
                }
            },
            nChains);
        progress.Done();
    }
    // Store final image computed with MLT
    ProgressReporter progress2(width * height / 256, "add film");
    auto sampleMax =
        std::max_element(std::begin(pixcelSamplers), std::end(pixcelSamplers));
    printf(" %.4f \n", *sampleMax);
	//生成采样分布图
    ParallelFor(
        [&](int i) {
            // Generate _i_th bootstrap sample
            Point2f pRaster(i / height, i % height);
            camera->samplefilm->AddSplat(
                pRaster, GetSamplePixcel(pixcelSamplers[i] / 200));
            Spectrum s = filmTsallis[i] * _b / _pixcelSamplers[i];
            // camera->film->AddSplat(pRaster, s);
            // if((int)pRaster.x == 532 && (int)pRaster.y == 691)
            //     std::cout<<"color = "<<s.ToString().c_str()<<std::endl;
            if ((i + 1) % 256 == 0) progress2.Update();
        },
        width * height);
    progress2.Done();
    camera->film->WriteImage(_b / mutationsPerPixel);
    camera->samplefilm->WriteImage(1);
}

MLTFixIntegrator *CreateMLTFixIntegrator(const ParamSet &params,
                                         std::shared_ptr<const Camera> camera,
                                         std::shared_ptr<Sampler> sampler) {
    int maxDepth = params.FindOneInt("maxdepth", 17);
    int nBootstrap = params.FindOneInt("bootstrapsamples", 100000);
    int64_t nChains = params.FindOneInt("chains", 1000);
    int mutationsPerPixel = params.FindOneInt("mutationsperpixel", 100);
    if (PbrtOptions.spp != 0) mutationsPerPixel = PbrtOptions.spp;
    mutationsPerPixel = mutationsPerPixel * 1.2f;
    int pixelSamplerNum = params.FindOneInt("pixelSamplerNum", 8);//计算信息熵的范围
    bool pixelArea = params.FindOneBool("pixelArea", false);
    bool useShannon = params.FindOneBool("useShannon", true);//是否使用香浓熵
    bool smapleImg = params.FindOneBool("smapleImg", false);//是否生成采样分布图
    Float tsallisRatio = params.FindOneFloat("tsallisRatio", 2.0f);//使用tsallis上时的Q
    Float largeStepProbability =
        params.FindOneFloat("largestepprobability", 0.3f);
    Float sigma = params.FindOneFloat("sigma", .01f);
    if (PbrtOptions.quickRender) {
        mutationsPerPixel = std::max(1, mutationsPerPixel / 16);
        nBootstrap = std::max(1, nBootstrap / 16);
    }
    return new MLTFixIntegrator(camera, maxDepth, nBootstrap, nChains,
                                mutationsPerPixel, sigma, largeStepProbability,
                                pixelSamplerNum, pixelArea, tsallisRatio,
                                sampler, useShannon, smapleImg);
}

}  // namespace pbrt
