
/*
    pbrt source code is Copyright(c) 1998-2016
                        Matt Pharr, Greg Humphreys, and Wenzel Jakob.

    This file is part of pbrt.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are
    met:

    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.

    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
    TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
    PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

#if defined(_MSC_VER)
#define NOMINMAX
#pragma once
#endif

#ifndef PBRT_INTEGRATORS_GMLT_H
#define PBRT_INTEGRATORS_GMLT_H

// integrators/mlt.h*
#include "pbrt.h"
#include "integrator.h"
#include "sampler.h"
#include "spectrum.h"
#include "film.h"
#include "rng.h"
#include <unordered_map>

namespace pbrt {

// GMLTSampler Declarations
class GMLTSampler : public Sampler {
  public:
    struct SpectrumSample {
        Spectrum L;
        Point2f p;
        SpectrumSample() : L(Spectrum(0)), p(Point2f(0,0)){}
        SpectrumSample(Spectrum L, Point2f p):L(L),p(p){}
    };
    struct PrimarySample {
        Float value = 0;
        // PrimarySample Public Methods
        void Backup() {
            valueBackup = value;
            modifyBackup = lastModificationIteration;
        }
        void Restore() {
            value = valueBackup;
            lastModificationIteration = modifyBackup;
        }
        void LargeRestore() { value = largeBackupValue; }
        void LargeBackup() { largeBackupValue = value; }

        // PrimarySample Public Data
        int64_t lastModificationIteration = 0;
        Float valueBackup = 0;
        int64_t modifyBackup = 0;
        int64_t largeBackupValue = 0;
    };
    // GMLTSampler Public Methods
    GMLTSampler(int mutationsPerPixel, int rngSequenceIndex, Float sigma,
               Float largeStepProbability, int streamCount)
        : Sampler(mutationsPerPixel),
          rng(rngSequenceIndex),
          sigma(sigma),
          largeStepProbability(largeStepProbability / 2),
          _largeStepProbability(largeStepProbability / 2),
          streamCount(streamCount),
          rngSequenceIndex(rngSequenceIndex) {}
    Float Get1D();
    Point2f Get2D();
    std::unique_ptr<Sampler> Clone(int seed);
    void StartIteration();
    void LargeAccept();
    void LargeReject();
    void Accept();
    void Reject();
    void StartStream(int index);
    void Set2D(float x, float y)
	{ 
		X[GetPreIndex()].value = x;
        X[GetCurIndex()].value = y;
	}
    void LightRestore() { LightValue = LightBackUp; }
    void LightBackup() { LightBackUp = LightValue; }
    void SetLightValue(Spectrum l, Point2f p) {
        LightValue.L = l;
        LightValue.p = p;
        lightSetNum++;
    }
    void SetLightValue(SpectrumSample ss)
	{
		LightValue = ss;
	}
    SpectrumSample GetLightValue() { return LightValue; }
    int GetNextIndex() {
        return streamIndex + streamCount * sampleIndex++;
    }
    int GetPreIndex() { return streamIndex + streamCount * (sampleIndex-1); }
    int GetCurIndex() { return streamIndex + streamCount * sampleIndex; }
    void SetJitter(bool isJitter) { jitter = isJitter; }
    bool IsJitter() { return jitter; }
    void SetLargeStepProbability(Float probability) {
        largeStepProbability =
            std::min(std::max(_largeStepProbability - probability,
                              _largeStepProbability / 2),
                     _largeStepProbability * 2);
    }
    void ResetLargeStepProbability() {
        largeStepProbability = _largeStepProbability;
    }
    int rngSequenceIndex;
    bool accept = false;
    std::vector<PrimarySample> X;
    int streamIndex, sampleIndex;
    int64_t currentIteration = 0;
    int64_t currentLargeIteration = 0;
    int64_t lastLargeInteration = 0;
    int64_t lastLargeStepIteration = 0;
    bool jitter = false;
    bool largeStep = true;
  protected:
    // GMLTSampler Private Declarations
    // GMLTSampler Private Methods
    void EnsureReady(int index);
    // GMLTSampler Private Data
    RNG rng;
    Float sigma, largeStepProbability, _largeStepProbability;
    const int streamCount;
    int64_t lightSetNum = 0;
    SpectrumSample LightBackUp;
    SpectrumSample LightValue;
};

// MLT Declarations
class GMLTIntegrator : public Integrator {
  public:
    // GMLTIntegrator Public Methods
    GMLTIntegrator(std::shared_ptr<const Camera> camera, int maxDepth,
                  int nBootstrap, int nChains, int mutationsPerPixel,
                  Float sigma, Float largeStepProbability)
        : camera(camera),
          maxDepth(maxDepth),
          nBootstrap(nBootstrap),
          nChains(nChains),
          mutationsPerPixel(mutationsPerPixel),
          sigma(sigma),
          largeStepProbability(largeStepProbability) {}
    void Render(const Scene &scene);
    Spectrum L(const Scene &scene, MemoryArena &arena,
               const std::unique_ptr<Distribution1D> &lightDistr,
               const std::unordered_map<const Light *, size_t> &lightToIndex,
               GMLTSampler &sampler, int k, Point2f *pRaster);

  protected:
    // GMLTIntegrator Private Data
    std::shared_ptr<const Camera> camera;
    const int maxDepth;
    const int nBootstrap;
    const int nChains;
    const int mutationsPerPixel;
    const Float sigma, largeStepProbability;
    std::vector<int> pixcelSamplers;
};

GMLTIntegrator *CreateGMLTIntegrator(const ParamSet &params,
                                   std::shared_ptr<const Camera> camera);

}  // namespace pbrt

#endif  // PBRT_INTEGRATORS_MLT_H
