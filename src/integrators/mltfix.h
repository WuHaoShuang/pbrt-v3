
/*
    pbrt source code is Copyright(c) 1998-2016
                        Matt Pharr, Greg Humphreys, and Wenzel Jakob.

    This file is part of pbrt.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are
    met:

    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.

    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
    TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
    PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

#if defined(_MSC_VER)
#define NOMINMAX
#pragma once
#endif

#ifndef PBRT_INTEGRATORS_MLTFix_H
#define PBRT_INTEGRATORS_MLTFix_H

// integrators/mlt.h*
#include "pbrt.h"
#include "integrator.h"
#include "sampler.h"
#include "spectrum.h"
#include "film.h"
#include "rng.h"
#include <unordered_map>

namespace pbrt {

// MLTFixSampler Declarations
class MLTFixSampler : public Sampler {
  public:
    // MLTFixSampler Public Methods
    MLTFixSampler(int mutationsPerPixel, int rngSequenceIndex, Float sigma,
               Float largeStepProbability, int streamCount)
        : Sampler(mutationsPerPixel),
          rng(rngSequenceIndex),
          rng2(rngSequenceIndex),
          sigma(sigma),
          largeStepProbability(largeStepProbability),
          _largeStepProbability(largeStepProbability),
          streamCount(streamCount) {}
    Float Get1D();
    Point2f Get2D();
    std::unique_ptr<Sampler> Clone(int seed);
    void SetJitter(bool isJitter);
    void StartIteration();
    void Accept();
    void Reject();
    void StartStream(int index);
    int GetNextIndex() { return streamIndex + streamCount * sampleIndex++; }
    bool IsJitter() { return jitter; }
    void SetLargeStepProbability(Float probability){ largeStepProbability = std::min(std::max(_largeStepProbability - probability, _largeStepProbability / 2), _largeStepProbability * 2); }
    void ResetLargeStepProbability(){ largeStepProbability = _largeStepProbability; }
  protected:
    // MLTFixSampler Private Declarations
    struct PrimarySample {
        Float value = 0;
        // PrimarySample Public Methods
        void Backup() {
            valueBackup = value;
            modifyBackup = lastModificationIteration;
        }
        void Restore() {
            value = valueBackup;
            lastModificationIteration = modifyBackup;
        }

        // PrimarySample Public Data
        int64_t lastModificationIteration = 0;
        Float valueBackup = 0;
        int64_t modifyBackup = 0;
    };

    // MLTFixSampler Private Methods
    void EnsureReady(int index);

    // MLTFixSampler Private Data
    RNG rng;
    RNG rng2;
    const Float sigma, _largeStepProbability;
    const int streamCount;
    std::vector<PrimarySample> X;
    int64_t currentIteration = 0;
    bool largeStep = true;
    bool jitter = false;
    int64_t lastLargeStepIteration = 0;
    int streamIndex, sampleIndex;
    Float largeStepProbability;
};

// MLT Declarations
class MLTFixIntegrator : public Integrator {
  public:
    // MLTFixIntegrator Public Methods
    MLTFixIntegrator(std::shared_ptr<const Camera> camera, int maxDepth,
                  int nBootstrap, int nChains, int mutationsPerPixel,
                  Float sigma, Float largeStepProbability, int pixelSamplerNum, bool pixelArea, Float tsallisRatio, std::shared_ptr<Sampler> sample, bool useShannon,
                  bool smapleImg)
        : camera(camera),
          maxDepth(maxDepth),
          nBootstrap(nBootstrap),
          nChains(nChains),
          mutationsPerPixel(mutationsPerPixel),
          sigma(sigma),
          largeStepProbability(largeStepProbability),
          pixelSamplerNum(pixelSamplerNum),
          pixelArea(pixelArea),
          tsallisRatio(tsallisRatio),
          useShannon(useShannon),
          smapleImg(smapleImg),
          _sampler(sample)  {
            Float temp = 1.0 / ((pixelSamplerNum + 1) * (pixelSamplerNum + 1) + 1);
            maxShannon = -(temp * (log(temp) / log(2.0)) * ((pixelSamplerNum + 1) * (pixelSamplerNum + 1) + 1));
          }
    void Render(const Scene &scene);
    Spectrum L(const Scene &scene, MemoryArena &arena,
               const std::unique_ptr<Distribution1D> &lightDistr,
               const std::unordered_map<const Light *, size_t> &lightToIndex,
               MLTFixSampler &sampler, int k, Point2f *pRaster, Float *misWeight);
    Float GetTsallis(Spectrum currentL, std::vector<Spectrum> sv);
    Float GetShannon(std::vector<Spectrum> sv);
  private:
    // MLTFixIntegrator Private Data
    std::shared_ptr<const Camera> camera;
    const int maxDepth;
    const int nBootstrap;
    const int nChains;
    const int mutationsPerPixel;
    const int pixelSamplerNum;
    const Float sigma, largeStepProbability;
    const bool pixelArea, useShannon, smapleImg;
    const Float tsallisRatio;
    Float maxShannon;
    std::shared_ptr<Sampler> _sampler;
};

MLTFixIntegrator *CreateMLTFixIntegrator(const ParamSet &params,
                                   std::shared_ptr<const Camera> camera, std::shared_ptr<Sampler> sampler);

}  // namespace pbrt

#endif  // PBRT_INTEGRATORS_MLT_H
